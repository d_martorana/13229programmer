﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lstShell = New System.Windows.Forms.ListBox()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.lblOperator = New System.Windows.Forms.Label()
        Me.txtOperator = New System.Windows.Forms.TextBox()
        Me.txtSerialNumber = New System.Windows.Forms.TextBox()
        Me.lblSerialNumber = New System.Windows.Forms.Label()
        Me.txtNomeFile = New System.Windows.Forms.TextBox()
        Me.lblNomeFile = New System.Windows.Forms.Label()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnNuova = New System.Windows.Forms.Button()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.picSfondo = New System.Windows.Forms.PictureBox()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSfondo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstShell
        '
        Me.lstShell.FormattingEnabled = True
        Me.lstShell.HorizontalScrollbar = True
        Me.lstShell.Location = New System.Drawing.Point(344, 88)
        Me.lstShell.Name = "lstShell"
        Me.lstShell.Size = New System.Drawing.Size(261, 134)
        Me.lstShell.TabIndex = 0
        '
        'btnStart
        '
        Me.btnStart.Enabled = False
        Me.btnStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.Location = New System.Drawing.Point(216, 184)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(122, 38)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "START"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'lblOperator
        '
        Me.lblOperator.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperator.Location = New System.Drawing.Point(18, 91)
        Me.lblOperator.Name = "lblOperator"
        Me.lblOperator.Size = New System.Drawing.Size(135, 20)
        Me.lblOperator.TabIndex = 2
        Me.lblOperator.Text = "Operator:"
        Me.lblOperator.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtOperator
        '
        Me.txtOperator.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperator.Location = New System.Drawing.Point(159, 88)
        Me.txtOperator.Name = "txtOperator"
        Me.txtOperator.Size = New System.Drawing.Size(179, 26)
        Me.txtOperator.TabIndex = 3
        '
        'txtSerialNumber
        '
        Me.txtSerialNumber.Enabled = False
        Me.txtSerialNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialNumber.Location = New System.Drawing.Point(159, 120)
        Me.txtSerialNumber.MaxLength = 16
        Me.txtSerialNumber.Name = "txtSerialNumber"
        Me.txtSerialNumber.Size = New System.Drawing.Size(179, 26)
        Me.txtSerialNumber.TabIndex = 5
        '
        'lblSerialNumber
        '
        Me.lblSerialNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerialNumber.Location = New System.Drawing.Point(22, 123)
        Me.lblSerialNumber.Name = "lblSerialNumber"
        Me.lblSerialNumber.Size = New System.Drawing.Size(131, 20)
        Me.lblSerialNumber.TabIndex = 4
        Me.lblSerialNumber.Text = "Serial Number:"
        Me.lblSerialNumber.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtNomeFile
        '
        Me.txtNomeFile.Enabled = False
        Me.txtNomeFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNomeFile.Location = New System.Drawing.Point(159, 152)
        Me.txtNomeFile.Name = "txtNomeFile"
        Me.txtNomeFile.Size = New System.Drawing.Size(179, 26)
        Me.txtNomeFile.TabIndex = 7
        '
        'lblNomeFile
        '
        Me.lblNomeFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNomeFile.Location = New System.Drawing.Point(26, 155)
        Me.lblNomeFile.Name = "lblNomeFile"
        Me.lblNomeFile.Size = New System.Drawing.Size(127, 20)
        Me.lblNomeFile.TabIndex = 6
        Me.lblNomeFile.Text = "Nome File:"
        Me.lblNomeFile.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMessage
        '
        Me.lblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(12, 17)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(379, 63)
        Me.lblMessage.TabIndex = 8
        Me.lblMessage.Text = "Insert operator name:"
        '
        'btnReset
        '
        Me.btnReset.Enabled = False
        Me.btnReset.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.Location = New System.Drawing.Point(18, 184)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(93, 38)
        Me.btnReset.TabIndex = 9
        Me.btnReset.Text = "RESET"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnNuova
        '
        Me.btnNuova.Enabled = False
        Me.btnNuova.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuova.Location = New System.Drawing.Point(117, 184)
        Me.btnNuova.Name = "btnNuova"
        Me.btnNuova.Size = New System.Drawing.Size(93, 38)
        Me.btnNuova.TabIndex = 10
        Me.btnNuova.Text = "NEW"
        Me.btnNuova.UseVisualStyleBackColor = True
        '
        'picLogo
        '
        Me.picLogo.Image = Global._13229Programmer.My.Resources.Resources.logoOrizBianco
        Me.picLogo.Location = New System.Drawing.Point(416, 14)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(182, 47)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLogo.TabIndex = 11
        Me.picLogo.TabStop = False
        '
        'picSfondo
        '
        Me.picSfondo.Location = New System.Drawing.Point(397, -2)
        Me.picSfondo.Name = "picSfondo"
        Me.picSfondo.Size = New System.Drawing.Size(220, 76)
        Me.picSfondo.TabIndex = 12
        Me.picSfondo.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 255)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.btnNuova)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.txtNomeFile)
        Me.Controls.Add(Me.lblNomeFile)
        Me.Controls.Add(Me.txtSerialNumber)
        Me.Controls.Add(Me.lblSerialNumber)
        Me.Controls.Add(Me.txtOperator)
        Me.Controls.Add(Me.lblOperator)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.lstShell)
        Me.Controls.Add(Me.picSfondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSfondo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstShell As ListBox
    Friend WithEvents btnStart As Button
    Friend WithEvents lblOperator As Label
    Friend WithEvents txtOperator As TextBox
    Friend WithEvents txtSerialNumber As TextBox
    Friend WithEvents lblSerialNumber As Label
    Friend WithEvents txtNomeFile As TextBox
    Friend WithEvents lblNomeFile As Label
    Friend WithEvents lblMessage As Label
    Friend WithEvents btnReset As Button
    Friend WithEvents btnNuova As Button
    Friend WithEvents picLogo As PictureBox
    Friend WithEvents picSfondo As PictureBox
End Class
