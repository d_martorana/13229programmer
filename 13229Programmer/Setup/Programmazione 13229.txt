1) Installare il programma 3.0.0.97_Programmer.exe
2) Collegare il programmatore jtag Lattice alla porta USB
	e aspettare che vengano caricati i driver.
3) Collegare e alimentare il target, piastra 13229.
4) Lanciare il programma Diamond Programmer e aspettare che rilevi l'hw
	deve comparire LCMXO2-256HC
5) Doppio click sulla colonna Operation sopra FLASH E,P,V e impostare
	a) Access mode:	Flash Programming Mode
	b) Operation:	FLASH Erase,Program,Verify
	c) Programming file:	Path/BUFF_CAN_1_01.jed
6) Cliccare sull'icona con la freccia verde sul chip per programmare la PLD
7) In caso di problemi nella videata di Output vengono delle scritte rosse.
8) Salvare con nome il profilo ottenuto e chiudere
9) Lanciare nuovamente Diamond Programmer selezionando
	"Open an Existing programmer project" 	per ripartire